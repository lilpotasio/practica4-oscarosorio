package clase;

import java.time.LocalDate;

public class Producto {
	//atributos
	String n_pedido;
	String tamano;
	boolean refresco_lata;
	boolean patatas;
	String tipo_carne;
	boolean queso;
	boolean salsa_blanca;
	boolean salsa_picante;
	LocalDate fecha;
	//constructor
	public Producto(String n_pedido) {
		this.n_pedido = n_pedido;	
	}
	//getter and setter
	public String getN_pedido() {
		return n_pedido;
	}

	public void setN_pedido(String n_pedido) {
		this.n_pedido = n_pedido;
	}

	public String getTamano() {
		return tamano;
	}

	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	public boolean isRefresco_lata() {
		return refresco_lata;
	}

	public void setRefresco_lata(boolean refresco_lata) {
		this.refresco_lata = refresco_lata;
	}

	public boolean isPatatas() {
		return patatas;
	}

	public void setPatatas(boolean patatas) {
		this.patatas = patatas;
	}

	public String getTipo_carne() {
		return tipo_carne;
	}

	public void setTipo_carne(String tipo_carne) {
		this.tipo_carne = tipo_carne;
	}

	public boolean isQueso() {
		return queso;
	}

	public void setQueso(boolean queso) {
		this.queso = queso;
	}

	public boolean isSalsa_blanca() {
		return salsa_blanca;
	}

	public void setSalsa_blanca(boolean salsa_blanca) {
		this.salsa_blanca = salsa_blanca;
	}

	public boolean isSalsa_picante() {
		return salsa_picante;
	}

	public void setSalsa_picante(boolean salsa_picante) {
		this.salsa_picante = salsa_picante;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	//to streing
	@Override
	public String toString() {
		return "Producto [n_pedido=" + n_pedido + ", tamano=" + tamano + ", refresco_lata=" + refresco_lata
				+ ", patatas=" + patatas + ", tipo_carne=" + tipo_carne + ", queso=" + queso + ", salsa_blanca="
				+ salsa_blanca + ", salsa_picante=" + salsa_picante + ", fecha=" + fecha + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}


