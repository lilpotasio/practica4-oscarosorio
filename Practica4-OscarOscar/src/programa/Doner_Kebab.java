 package programa;
 
 import java.util.Scanner;
 
 import clase.Pedido;
 import clase.Producto;
 
 public class Doner_Kebab {
	 
	 public static void main(String[] args) {
		 //creo todas las variables fuera del bucle
		 	String sino;
		 	double precio=0;
			String vrefresco_lata;
			boolean refresco_lata;
			String tamano;
			String vpatatas;
			boolean patatas;
			String vqueso;
			boolean queso;
			String vsalsa_blanca;
			boolean salsa_blanca;
			String vsalsa_picante;
			boolean salsa_picante;
			String tipo_carne;
			String n_pedido;
			int n_pedido_int;
			int select;
			int select2;
			int contador=1;
			String si="si";
			String no="no";
			Scanner input = new Scanner(System.in);
			System.out.println("Amigo, bienvenido a d�ner kebab, le atiende lilpotasio �Qu� quiere pedir?");
			//pido MaxPedidos
			System.out.println("Cuantos pedidos va a realizar amigo?");
			int MaxPedidos = input.nextInt();
			//declaro MaxPedidos
			Pedido pedido = new Pedido(MaxPedidos);
			//Creo un bucle para que se repita el menu hasta que se seleccione saqlir
			do {
				System.out.println("Elige una opci�n amigo");
				System.out.println("1- Hacer pedido/s"
						+ "\n2- Consultar pedido"
						+ "\n3- Cancelar pedido"
						+ "\n4- Ver todos los pedidos"
						+ "\n5- Editar pedido"
						+ "\n6- Listar por"
						+ "\n7- Comprobar si tienes descuento"
						+ "\n8- Salir");
				select = input.nextInt();
				
				switch (select) {
				case 1:
					//metodo altaPedido
					input.nextLine();
					System.out.println("De qu� tama�o quieres tu pedido? Amigo"
							+ "\nM, L, XL");
					tamano = input.nextLine();
					
					System.out.println("Amigo, quieres un refresco lata con tu pedido? \nsi/no");
					vrefresco_lata = input.nextLine();
					if (vrefresco_lata.compareToIgnoreCase(si)==0) {
						refresco_lata=true;
					}
					else {
						refresco_lata=false;
					}
					System.out.println("Amigo, quieres patatas con tu pedido? \nsi/no");
					vpatatas = input.nextLine();
					if (vpatatas.compareToIgnoreCase(si)==0) {
						patatas=true;
					}
					else {
						patatas=false;
					}
					System.out.println("De qu� quieres el kebab? Amigo \nternera/pollo/mixto");
					tipo_carne = input.nextLine();
					System.out.println("Lo quieres con queso? Amigo \nsi/no");
					vqueso = input.nextLine();
					if (vqueso.compareToIgnoreCase(si)==0) {
						queso=true;
					}
					else {
						queso=false;
					}
					System.out.println("Salsa blanca? \nsi/no");
					vsalsa_blanca = input.nextLine();
					if (vsalsa_blanca.compareToIgnoreCase(si)==0) {
						salsa_blanca=true;
					}
					else {
						salsa_blanca=false;
					}
					System.out.println("Salsa picante? \nsi/no");
					vsalsa_picante = input.nextLine();
					if (vsalsa_picante.compareToIgnoreCase(si)==0) {
						salsa_picante=true;
					}
					else {
						salsa_picante=false;
					}
					n_pedido=String.valueOf(contador);
					pedido.altaPedido(n_pedido, tamano, refresco_lata, patatas, tipo_carne, queso, salsa_blanca, salsa_picante);
					System.out.println("FELICIDADES SU PEDIDO NUMERO "+contador+" SE COMPLETO EXITOSAMENTE");
					
					contador++;
					
					break;
				case 2:
					//metodo buscarPedido
					System.out.println("Cual es tu numero de pedido amigo?");
					n_pedido_int= input.nextInt();
					n_pedido=String.valueOf(n_pedido_int);
					pedido.buscarPedido(n_pedido);
					break;
				case 3:
					//metodo eliminarPedido
					System.out.println("Cual es el numero de pedido del pedido que desea eliminar amigo?");
					n_pedido_int=input.nextInt();
					n_pedido=String.valueOf(n_pedido_int);
					pedido.eliminarPedido(n_pedido);
					break;
				case 4:
					//metodo listarPedidos
					System.out.println("A continuacion se le mostraran todos los pedidos realizados");
					pedido.listarPedidos();
					break;
				case 5:
					//creo menu para que el cliente pueda cambiar lo que desee de su pedido
					System.out.println("Cual es el numero de pedido del pedido que quiere cambiar?");
					n_pedido_int=input.nextInt();n_pedido=String.valueOf(n_pedido_int);
					n_pedido=String.valueOf(n_pedido_int);
					if (n_pedido_int<=MaxPedidos && n_pedido_int>=1) {
						
						do {
							System.out.println("Que desea cambiar?"
									+ "\n1- Tama�o"
									+ "\n2- Patatas"
									+ "\n3- Refresco lata"
									+ "\n4- Tipo de carne"
									+ "\n5- Queso"
									+ "\n6- Salsa blanca"
									+ "\n7- Sala picante"
									+ "\n8- Volver");
							select2 =input.nextInt();
							
							switch (select2) {
							case 1:
								//metodo cambiarTamano
								input.nextLine();
								System.out.println("De qu� tama�o quieres tu pedido? Amigo"
										+ "\nM, L, XL");
								tamano = input.nextLine();
								pedido.cambiarTamano(n_pedido, tamano);
								break;
							case 2:
								//metodo cambiarPatatas
								input.nextLine();
								System.out.println("Amigo, quieres patatas con tu pedido? \nsi/no");
								vpatatas = input.nextLine();
								if (vpatatas.compareToIgnoreCase(si)==0) {
									patatas=true;
								}
								else {
									patatas=false;
								}
								pedido.cambiarPatatas(n_pedido, patatas);
								break;
							case 3:
								//metodo cambiarRefresco_lata
								input.nextLine();
								System.out.println("Amigo, quieres un refresco lata con tu pedido? \nsi/no");
								vrefresco_lata = input.nextLine();
								if (vrefresco_lata.compareToIgnoreCase(si)==0) {
									refresco_lata=true;
								}
								else {
									refresco_lata=false;
								}
								pedido.cambiarRefresco_lata(n_pedido, refresco_lata);
								break;
							case 4:
								//metodo cambiarTipo_carne
								input.nextLine();
								System.out.println("De qu� quieres el kebab? Amigo \nternera/pollo/mixto");
								tipo_carne = input.nextLine();	
								pedido.cambiarTipo_carne(n_pedido, tipo_carne);
								break;
							case 5:
								//metodo cambiarQueso
								input.nextLine();
								System.out.println("Lo quieres con queso? Amigo \nsi/no");
								vqueso = input.nextLine();
								if (vqueso.compareToIgnoreCase(si)==0) {
									queso=true;
								}
								else {
									queso=false;
								}
								pedido.cambiarQueso(n_pedido, queso);
								break;							
							case 6:
								//metodo cambiarSalsa_blanca
								input.nextLine();
								System.out.println("Salsa blanca? \nsi/no");
								vsalsa_blanca = input.nextLine();
								if (vsalsa_blanca.compareToIgnoreCase(si)==0) {
									salsa_blanca=true;
								}
								else {
									salsa_blanca=false;
								}		
								pedido.cambiarSalsa_blanca(n_pedido, salsa_blanca);
								break;
							case 7:
								//metodo cambiarSalsa_picante
								input.nextLine();
								System.out.println("Salsa picante? \nsi/no");
								vsalsa_picante = input.nextLine();
								if (vsalsa_picante.compareToIgnoreCase(si)==0) {
									salsa_picante=true;
								}
								else {
									salsa_picante=false;
								}
								pedido.cambiarSalsa_picante(n_pedido, salsa_picante);
								break;
							default:
								break;
							}
						} while (select2!=8);
					} else {
						System.out.println("ERROR OPCION SELECCIONADA INCORRECTA");
					}
					
					break;
				case 6:
					//menu para elegir metodo listar por
					System.out.println("Listar por:"
							+ "\n1- tipo carne"
							+ "\n2- tamano");
					select2=input.nextInt();
					switch (select2) {
					case 1:
						//metodo listarPedido_tipo_carne
						input.nextLine();
						System.out.println("introduce el tipo de carne pollo/ternera/mixto");
						tipo_carne=input.nextLine();
						pedido.listarPedido_tipo_carne(tipo_carne);
						break;
					case 2:
						//metodo listarPedido_tamano
						input.nextLine();
						System.out.println("introduce el tama�o M/L/XL");
						tamano=input.nextLine();
						pedido.listarPedido_tamano(tamano);
						break;
					default:
						System.out.println("ERROR OPCION SELECCIONADA INCORRECTA");
						break;
					}
					break;
				//metodo para ver si tienes un kebab gratis
				case 7:
					System.out.println("Escribe el numero de tu pedido y comprueba si tienes un kebab gratis");
					n_pedido_int=input.nextInt();
					n_pedido=String.valueOf(n_pedido_int);
					pedido.kebabGratis(n_pedido);
					break;
				//metodo ver precio
				case 8:
					System.out.println("Escribe el numero del pedido que quieres consultar");
					n_pedido_int=input.nextInt();
					n_pedido=String.valueOf(n_pedido_int);
					precio=pedido.verPrecio(n_pedido);
					System.out.println("Su pedido cuesta "+precio);					
					break;
				//metodo comprobar descuento
				case 9:
					System.out.println("ATENCION!!! SI NO HAS ELEGIDO LA OPCION VER PRECIO ANTES QUE ESTA NO SE TE DARA DESCUENTO"
							+ "\n Has elegido la opci�n ver precio anteriormente? si/no");
					sino=input.nextLine();
					if (sino==si) {
						System.out.println("Cual es el numero de tu pedido?");
						n_pedido_int=input.nextInt();
						n_pedido=String.valueOf(n_pedido_int);
						pedido.comprobarDescuento(n_pedido, precio);
					} else {
						System.out.println("Selecciona la opcion ver precios y vuelve a seleccionar esta opcion, gracias");
					}
					break;
				default:
					System.out.println("ERROR OPCION SELECCIONADA ERRONEA");
					break;
				}
				
			} while (select!=10);
	//ciero el scanner
	input.close();
	
 	}
 
 }